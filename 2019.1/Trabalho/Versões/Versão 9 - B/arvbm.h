#include "adiciona.h"
#include "pizza.h"

#define TAMFOLHA sizeof(long int)+sizeof(int)+sizeof(long int)*(T*2-1)+sizeof(long int)
//#define TAMINDICE sizeof(int)+sizeof(int)*(2*T-1)+sizeof(long int)+sizeof(long int)*(2*T)


//A ESTRURA DA ARVORE FOI MODIFICADA
//AINDA VOU VER QUAL DAS VARIAS EU NÃO USO
typedef struct arvbm{
  int nchaves, folha, *chave;
  struct arvbm **filho, *pai, *ant, *prox; //ant e prox apenas para as folhas
  struct pizza **pizzas;
  long int posPai, posProx, posAnt, *posPizza, *posFilho, posMeu;
}TABM;

TABM *inicializa(void);

TABM *cria(TPizza *p, int t);

void imprime(TABM *a, int t);

void libera(TABM *a, int t);

TABM* addpai(TABM *a, int t); //indica o pai de cada nó

TABM* addproximo(TABM *a, TABM *prec, int t); //indica o proximo nó

TABM* busca(TABM *a, int cod, int t);

TABM* divisao(TABM *a, int i, TABM *y, int t);

TABM* insNCompleta(TABM *a, TPizza *p, int t);

TABM* insere(TABM *a, TPizza *p, int t); //insere do arquivo inicial para arvore

void libera(TABM *a, int t);

void imprime_arvore(TABM *a, int andar, int t);

TABM* excluiNCompleto(TABM *x, int info, int t);

TABM* concatenaIrmao(TABM *a, int i, TABM *y, int cmp, int t);

TABM* redistribui(TABM *a, int cmp, TABM *y, int t);

TABM* exclui(TABM *a, int info, int t);

// ------------------------- BARBARA --------------------------------------

//FUNÇÃO PARA ESCREVER OS NÓS FOLHAS NO AQUIVO DE DADOS
int escreveDados(TABM *a, int T);

//ESCREVE OS NOS INTERNOS NO ARQUIVO DE INDICE
long int escreveIndice(TABM *a, int T, int j);

//ESCREVE TODAS AS FOLHAS DA DIREITA PARA ESQUERDA
int escreveFolhas(TABM *a, int t);

//ESCREVE A ERVORE TODA NOS AQRUIVOS CORRESPONDENTES
long int escreveArvore(TABM *a, int t);

//ABRE OS TRES AQUIVOS QUE VAMOS UTILIZAR ("dados.dat", "indice.dat", "pizza,dat")
void abreAquivos();

//FOR DE LER OS INDICES DO ARQUIVO - ORDEM DE LEITURA
void lerIndice(FILE *arquivo, int t);

//FORMA DE LER OS DADOS DO AQUIVO - ORDEM DE LEITURA
void lerDados(FILE *arquivo, int t);

//LER PIZZA
int lerPizzaArquivo(FILE *arquivo);

//Busca a pizza no arquivo de folhas
long int BuscaDados(long int end, int codigo, int t);

//Buca a pizza e retorna o endereço da pizza no arquivo pizza
long int BuscaArquivo(long int end, int codigo, int t, FILE *arquivo);