#include "arvbm.h"

void MenuPrincipal()
{
	printf("\nMENU PRINCIPAL:\n");
	printf("1 - Inserir nova pizza\n");
	printf("2 - Remover pizza\n"); 
	printf("3 - Bucar pizza\n");
	printf("0 - Sair\n");
	printf("Digite a opção desejada: ");
}

void CodigoCategoria()
{
	printf("\n1 - Codigo\n");
	printf("2 - Categoria\n");
	printf("Digite a opção desejada: ");
}

void PizzaCodMenu()
{
	printf("\n1 - Imprimir\n");
	printf("2 - Alterar infromações\n");
	printf("3 - Voltar\n");
	printf("Digite a opção desejada: ");
}

void ModificaMenu()
{
	printf("\n1 - Nome\n");
	printf("2 - Categoria\n");
	printf("3 - Preço\n");
	printf("Digite a opção desejada: ");
}


void main(void){

	int op, t, aux;
	printf("Digite o fator de ramificação (t) da árvore B+: ");
	scanf("%d", &t);

	TABM *arv = NULL; //cria arvore
    FILE *o = fopen("dados_iniciais.dat", "rb"); //abre arquivo
    if(!o) exit(1);
    rewind(o); //ter certeza que o ponteiro esta no inicio do arquivo
    TPizza *p = le_pizza(o);
    int i = 0;
    while(p){
    	arv = insere(arv, p, t);
        p = le_pizza(o);
    }

    abreAquivos();
    long int raiz = escreveArvore(arv, t);

	do{

		MenuPrincipal();
		scanf("%d", &op);

		if(op == 1){
			//inserir
			printf("\nDesculpa, mas essa opção não esta disponivel.\n");

		}else if(op == 2){
			//remover elemento
			printf("\nDesculpa, mas essa opção não esta disponivel.\n");

		}else if(op == 3){
			//Bucar pizza
			CodigoCategoria();
			scanf("%d", &op);
			if(op == 1){
				printf("\nDigite o codigo: ");
				scanf("%d", &aux);

				FILE *arquivo = fopen("indice.dat", "rb+");
				rewind(arquivo);
				long int end = BuscaArquivo(0, aux, t, arquivo);
				FILE *arqpizza = fopen("pizza.dat", "rb+");

			    if(end >= 0){

			    	PizzaCodMenu();
					scanf("%d", &op);

					fseek(arqpizza, end , SEEK_SET);
				    TPizza *piz = le_pizza(arqpizza);
					if(op == 1){
				        fclose(arqpizza);
				        fclose(arquivo);
				        printf("\n");
				        imprime_pizza(piz);

					}else if(op == 2)
					{							
						ModificaMenu();
						scanf("%d", &op);
						if(op == 1){
							//Nome
							char novo_nome[50];
							printf("\nNovo nome: ");
							scanf(" %50[^\n]", novo_nome);
							TPizza *nova = pizza(piz->cod, novo_nome, piz->descricao, piz->preco);
							fseek(arqpizza, end , SEEK_SET);
							imprime_pizza(nova);
							salva_pizza(nova, arqpizza);
							fclose(arqpizza);
				        	fclose(arquivo);

						}else if(op == 2)
						{
							//Categoria
							char novo_cat[20];
							printf("\nNova descrição: ");
							scanf(" %20[^\n]", novo_cat);
							TPizza *nova = pizza(piz->cod, piz->nome, novo_cat, piz->preco);
							fseek(arqpizza, end , SEEK_SET);
							printf("\n");
							imprime_pizza(nova);
							salva_pizza(nova, arqpizza);
							fclose(arqpizza);
				        	fclose(arquivo);

							
						}else if(op == 3){
							//preço
							float novo_preco;
							printf("\nNovo preço: " );
							scanf("%f", &novo_preco);
							TPizza *nova = pizza(piz->cod, piz->nome, piz->descricao, novo_preco);
							fseek(arqpizza, end , SEEK_SET);
							printf("\n");
							imprime_pizza(nova);
							salva_pizza(nova, arqpizza);
							fclose(arqpizza);
				        	fclose(arquivo);


						}else printf("Opção invalida.\n");

					}else {
						fclose(arquivo);
						fclose(arqpizza);
						printf("Opção não encontrada, desculpe.\n");
					}
			        
			    } else printf("\nCodigo não exite\n");


			}else if(op == 2){
				char categ[20];
				printf("\nDigite a categoria: ");
				scanf(" %20[^\n]", categ);
				listaCategoria(categ, t);

			} else{
				printf("Opção não encontrada.\n");
			}

		}else if(op == 0)
		{
			printf("\nObrigada por utilizar nosso progrma.\n");
			break;

		}else{
			printf("\nDigite novamente, opção invalida.\n");
		}

	}while(op != 0);

}