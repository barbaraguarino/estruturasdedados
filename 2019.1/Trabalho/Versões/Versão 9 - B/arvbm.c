#include "arvbm.h"

int vez;

//------------------------ Criação e inicialização --------------------------------------

TABM *inicializa(void){
  return NULL;
}

//A FUNÇÃO CRIA FOI MODIFICADA
TABM *cria(TPizza *p, int T){
  TABM* novo = (TABM*)malloc(sizeof(TABM)); //aloca posição na memória
  //add os valores padrões:
  novo->nchaves = 0;
  novo->folha = 1;
  novo->chave = (int*)malloc(sizeof(int)*(T*2-1));
  novo->pizzas = (TPizza**)malloc(sizeof(TPizza*)*(T*2-1));
  novo->pai = NULL;
  novo->ant = NULL;
  novo->prox = NULL;
  novo->filho = (TABM**)malloc(sizeof(TABM*)*T*2);
  novo->posPai = -2;
  novo->posProx = -1;
  novo->posAnt = -1;
  novo->posMeu = -1;

  novo -> posPizza = (long int*)malloc(sizeof(long int)*(T*2-1));
  novo -> posFilho = (long int*)malloc(sizeof(long int)*(T*2));

  int i;

  for(i = 0; i<T*2-1; i++) novo->posPizza[i] = -1;
  for(i = 0; i<2*T; i++) novo->posFilho[i] = -1;

  for(int i=0; i<(T*2); i++)
    novo->filho[i] = NULL;
    
  for(int i = 0; i < (T*2-1); i++){
    novo->pizzas[i] = NULL;
    novo->chave[i] = INT_MIN;
  }
  
  novo->pizzas[0] = p;
  if(p){
    novo->chave[0] = p->cod;
    novo->nchaves = 1;
  }
  return novo;
}
//---------------------------------------------------------------------------------------

//---------------------------------- Impressões -----------------------------------------

//imprime as pizzas
void imprime(TABM *a, int t){ 
  if(a){ 
    if(!a->folha)
      imprime(a->filho[0], t);
    else{
      for(int i = 0; i < a->nchaves; i++)
        imprime_pizza(a->pizzas[i]);
    }
    if(a->prox)
      imprime(a->prox, t);
  }
}

//imprime a arvore inteira
void imprime_arvore(TABM *a, int andar, int t){
  if(a){
    int i,j;
    for(i=0; i<=a->nchaves-1; i++){
      imprime_arvore(a->filho[i],andar+1, t);
      for(j=0; j<=andar; j++) printf("      ");
      printf("%d\n", a->chave[i]);
    }
    imprime_arvore(a->filho[i],andar+1, t);
  }
}
//---------------------------------------------------------------------------------------


//Não está funcionando (?????)
TABM* addpai(TABM *a, int t){ //adiciona o pai do nó
  TABM *ant = a;
  if(a->folha) return a;
  for(int i = 0; i <= a->nchaves; i++){
    a->filho[i]->pai = ant;
    a->filho[i] = addpai(a->filho[i], t);
  }
  return a;
}

//Não está funcionando (?????)
TABM* addproximo(TABM *a, TABM *prec, int t){ //adiciona todos os próximos de uma vez
  //prec == nó que precede o nó atual
  if(!a) return NULL;

  if(a->folha){
    a->prox = NULL;
    return a;
  }else if(a->filho[0]->folha){
    if(prec){
      prec->prox = a->filho[0];
      a->filho[0]->ant = prec; //aproveita para add o ant de a
    }
    for(int i = 1; i < 2*t; i++){
      a->filho[i]->prox = a->filho[i-1];
      a->filho[i-1]->ant = a->filho[i]; //aproveita para add o ant de a
    }
  }
  else{
    for(int i = 0; i < 2*t; i++){
      if(a->filho != 0)
        a->filho[i] = addproximo(a->filho[i], a->filho[i-1]->filho[(a->filho[i-1]->nchaves)], t);
      else
        a->filho[i] = addproximo(a->filho[i], NULL, t);
    }
  }
  if(!a->pai){
    TABM *b = a;
    while(b->filho[b->nchaves]) b = b->filho[b->nchaves];
    b->prox = NULL;
  }
  return a;
}

TABM* busca(TABM *a, int cod, int t){
  if (!a) return NULL;
  int i = 0;
  if(!a->folha){
    while((a->nchaves > i)&&(cod >= a->chave[i])) i++;
    return busca(a->filho[i], cod, t);
  }
  return a; //retorna o nó cujo valor deveria estar, o valor estando presente na arvore ou não
}

//---------------------------- INSERÇÃO NA ARVORE ----------------------------------------P

TABM *divisao(TABM *x, int i, TABM *y, int t){
  TABM *z = cria(NULL, t);
  z->folha = y->folha;
  z->pai = x;
  int j;
  if(!y->folha){
    z->nchaves = t-1;
    for(j=0;j<t-1;j++) z->chave[j] = y->chave[j+t];
    for(j=0;j<t;j++){
      z->filho[j] = y->filho[j+t];
      y->filho[j+t] = NULL;
    }
  }
  else {
    z->nchaves = t; //z possuir� uma chave a mais que y se for folha
    for(j=0;j < t;j++) { //Caso em que y � folha, temos q passar a info para o n� da direita
      z->chave[j] = y->chave[j+t-1];
      z->pizzas[j] = y->pizzas[j+t-1];
    }
    y->prox = z;
    z->ant = y;
  }
  y->nchaves = t-1;
  //----------------------------------------------------------
  for(j=x->nchaves; j>=i; j--) {
    x->filho[j+1]=x->filho[j]; 
  }
  //----------------------------------------------------------
  x->filho[i] = z;
  //----------------------------------------------------------
  for(j=x->nchaves; j>=i; j--) {
    x->chave[j] = x->chave[j-1];
    x->pizzas[j] = x->pizzas[j-1];
  }
  //----------------------------------------------------------
  x->chave[i-1] = y->chave[t-1];
  x->nchaves++;
  return x;
}

TABM* insNCompleta(TABM *a, TPizza *p, int t){ //a unica alteração é adicionar as pizzas se forem folhas
  int i = a->nchaves-1;
  if(a->folha){
    while((i >= 0)&&(p->cod < a->chave[i])){
      a->chave[i+1] = a->chave[i];
      a->pizzas[i+1] = a->pizzas[i];
      i--;
    }
    a->chave[i+1] = p->cod;
    a->pizzas[i+1] = p;
    a->nchaves++;
    return a;
  } else{
    while((i >= 0)&&(p->cod < a->chave[i])) i--;
    i++;
    if(a->filho[i]->nchaves == (t*2-1)){
      a = divisao(a, (i+1), a->filho[i], t);
      if(p->cod > a->chave[i]) i++;
    }
    a->filho[i] = insNCompleta(a->filho[i], p, t);
  }
  return a;
}

TABM* insere(TABM *a, TPizza *p, int t){
  if(!a){
    a = cria(p, t);
    return a;
  }
  if(a->nchaves == (t*2-1)){
    TABM *novo = cria(NULL, t);
    novo->folha = 0;
    novo->filho[0] = a;
    if(a->pai){
      int i = 0;
      while(a->pai->filho[i] != a) i++;
      a->pai->filho[i] = novo;
      novo->pai = a->pai;
    }
    a->pai = novo;
    novo = divisao(novo, 1, a, t);
    novo = insNCompleta(novo, p, t);
    return novo;
  }
  a = insNCompleta(a, p, t);
  return a;
}
//---------------------------------------------------------------------------------------

//--------------------------- Funções de Deleções ---------------------------------------
//Libera o nó
void libera(TABM *a, int t){ 
  if(a){
    for(int i = 0; i <= a->nchaves; i++){
      libera(a->filho[i], t);
    }
    /*for(int i = 0; i < a->nchaves; i++){ e
      free(a->pizzas[i]); f(i)
    } ok */
    free(a->filho);
    free(a->pizzas);
    free(a->chave);
    free(a->posPizza);
    free(a->posFilho);
    free(a);
  }
}

//------------------------- possivelmente dando certo ----------------------------------

TABM* excluiNCompleto(TABM *x, int info, int t){
  int i = 0;
  while((i < x->nchaves)&&(info != x->chave[i]))i++;
  while(i < x->nchaves-1){ d
    x->chave[i] = x->chave[i+1];
    if(x->folha) x->pizzas[i] = x->pizzas[i+1];
    i++;
  }
  x->chave[x->nchaves-1] = INT_MIN;
  x->nchaves--;
  return x;
}

TABM* concatenaIrmao(TABM *a, int i, TABM *y, int cmp, int t){
  //a = irmao q recebe; i = 1ª chave do pai maior q o menor filho; y = irmao q some; cmp = qual irmao é maior; t = T
  TABM *pai = a->pai;
  if(cmp == 1){ //a é o irmão menor
    for(int j = 0; j < t-1; j++){
      a->chave[a->nchaves] = y->chave[j];
      y->chave[j] = INT_MIN;
      y->nchaves--;
      a->nchaves++;
    }
    for(int j = i; j < pai->nchaves-1; j++){
      pai->chave[j] = pai->chave[j+1];
      pai->filho[j+1] = pai->filho[j+2];
    }
    pai->chave[pai->nchaves-1] = INT_MIN;
    pai->filho[pai->nchaves] = NULL;
    pai->nchaves--;
    libera(y, t);
  } else{ //y é o irmão menor
    for(int j = a->nchaves; j > 0; j--)
      a->chave[j] = a->chave[j-1];
    for(int j = 0; j < t-1; j++){
      a->chave[0] = y->chave[y->nchaves-1];
      a->nchaves++;
    }
    for(int j = i; j < pai->nchaves; j++){
      pai->chave[j] = pai->chave[j+1];
      pai->filho[j] = pai->filho[j+1];
    }
    pai->chave[pai->nchaves-1] = INT_MIN;
    pai->filho[pai->nchaves] = NULL;
    pai->nchaves--;
    libera(y, t);
  }

  //excluir pai
  if(pai->nchaves == t-1){
    TABM *avo = pai->pai;
    int j = 0;

    while((j >= avo->nchaves)&&(avo->filho[j] != pai)) i++;

    if(i != 0){//se pai não é primeiro filho de avo
      if(avo->filho[j-1]->nchaves + pai->nchaves < 2*t-1){
        TABM *ant = avo->filho[j-1];
        ant->chave[ant->nchaves-1] = avo->chave[j-1];
        ant->chave++;

        ant->filho[ant->nchaves] = pai->filho[0];

        for(int l = j-1; l < avo->nchaves-2; l++)
          avo->chave[l] = avo->chave[l+1];
        avo->chave[avo->nchaves-1] = INT_MIN;
        avo->nchaves--;
        if(avo->nchaves <= 0) libera(avo, t);

        for(int l = 0; l < pai->nchaves-1; l++){
          ant->chave[ant->nchaves] = pai->chave[l];
          ant->filho[ant->nchaves+1] = pai->filho[l+1];
          ant->nchaves++;
          pai->nchaves--;
        }
        for(int l = 0; l < pai->nchaves; l++) //preparar para libera
          pai->filho[l] = NULL;
        libera(pai, t);
      }
    } else{
      if(avo->filho[j+1]->nchaves + pai->nchaves < 2*t-1){
        TABM *prox = avo->filho[j+1];
        pai->chave[pai->nchaves-1] = avo->chave[j-1];
        pai->chave++;

        pai->filho[pai->nchaves] = prox->filho[0];

        for(int l = j-1; l < avo->nchaves-2; l++)
          avo->chave[l] = avo->chave[l+1];
        avo->chave[avo->nchaves-1] = INT_MIN;
        avo->nchaves--;
        if(avo->nchaves <= 0) libera(avo, t);

        for(int l = 0; l < prox->nchaves-1; l++){
          pai->chave[pai->nchaves] = prox->chave[l];
          pai->filho[pai->nchaves+1] = prox->filho[l+1];
          pai->nchaves++;
          prox->nchaves--;
        }
        for(int l = 0; l < pai->nchaves; l++) //preparar para libera
          pai->filho[l] = NULL;
        libera(prox, t);
      }
    }
  }

  libera(a, t);
  if(y) return y;
  else return a;
}

TABM* redistribui(TABM *a, int cmp, TABM *y, int t){
  //a = irmão q recebera; cmp = qual irmão é maior; y = irmão que doara; t = t
  TABM *menor;
  if(cmp == 1) menor = a;
  else menor = y;

  TABM *pai = a->pai;
  int i = 0;
  while((i <= a->nchaves)&&(pai->filho[i] != menor))i++; //descobre indice do menor

  if(menor == a){ //se oq recebe está a esquerda
    a->chave[a->nchaves] = y->chave[0];
    a->nchaves++;
    for(int j = 0; j < y->nchaves-2; j++)
      y->chave[j] = y->chave[j+1];
    y->chave[y->nchaves-1] = INT_MIN;
    y->nchaves--;
    pai->chave[i] = y->chave[0];

  } else if(menor = y){ //se oq recebe está a direita
    for(int j = a->nchaves; j > 0; j--)
      a->chave[j] = a->chave[j-1];
    a->chave[0] = y->chave[y->nchaves-1];
    a->nchaves++;
    pai->chave[i] = y->chave[y->nchaves-1];
    y->chave[y->nchaves-1] = INT_MIN;
    y->nchaves--;
  }
  return pai; //retorna o pai dos dois filhos
}

TABM* exclui(TABM *a, int info, int t){
  TABM *x = busca(a, info, t);

  //se a informação não existir no nó retorna NULL
  int i = 0;
  while((i < x->nchaves)&&(info > x->chave[i]))i++;
  if(x->chave[i] != info) return a;

  //se o nó tem mais de t-1 chaves
  if(x->nchaves > t-1){
    x = excluiNCompleto(x, info, t);
    return a;
  }
  //casos 3:
  if(x->nchaves == t-1){
    if(x->folha){
      printf("Nó da árvore:\n");
      imprime_arvore(x, 0, t);
      TABM *pai = x->pai;
      int j = 0;
      TABM *anterior = NULL, *proximo = NULL;
      if(pai){
        while((j < pai->nchaves)&&(pai->filho[j] != pai)) j++;
        if(j != 0) anterior = pai->filho[j-1];
        if(j != pai->nchaves-1) proximo = pai->filho[j+1];
      }
      //caso 3a - Redistribuição:
      if((anterior)&&(x->nchaves + anterior->nchaves >= 2*t-1)){ d
        pai = redistribui(x, 0, anterior, t); 
        return a;
      } else if((proximo)&&(x->nchaves + proximo->nchaves >= 2*t-1)){
        TABM *pai = x->pai;d
        pai = redistribui(x, 1, x->prox, t);
        return a;
      }
      //caso 3a - Concatenação:
      //concatena com irmão da esquerda
      else if((anterior)&&(x->nchaves + anterior->nchaves < 2*t-1)){
        j = 0;
        while((j <= pai->nchaves)&&(pai->filho[j] != anterior)) j++;
        anterior = concatenaIrmao(anterior, j, x, 1, t);
        anterior = excluiNCompleto(anterior, info, t);
        return a;
      } //concatena com irmão da direita
      else if((proximo)&&(x->nchaves + proximo->nchaves < 2*t-1)){
        j = 0;
        while((j <= pai->nchaves)&&(pai->filho[j] != x)) j++;
        x = concatenaIrmao(x, j, proximo, 0, t);
        x = excluiNCompleto(x, info, t);
        return a;
      }
    }
  }  
  return a;
}

//----------------------- retirando na memoria ------------------------------------------

/*int ArqExcluiNCompl(char *arq, int info, int t){ //pega o arquivo dos dados
  FILE *f = fopen(arq, "rb+");
  long int dad = sizeof(int) + ((2*t-1)*sizeof(int)) + ((2*t-1)*sizeof(long int)) + sizeof(long int);
  long int atual = ftell(arq), fim;
  fseek(arq, 0, SEEK_END);
  fim = ftell(arq);
  fseek(arq, atual, SEEK_SET);
  while(atual < fim){
    int nch, *ch = (int*) malloc(sizeof(int)*(2*t-1));
    long int prox, *pizza = (long int*) malloc(sizeof(long int)*(2*t-1));
    fread(&nch, sizeof(int), 1, f);
    fread(ch, sizeof(int), sizeof(ch)/sizeof(int), f);
    fread(pizza, sizeof(long int), sizeof(pizza)/sizeof(long int), f);
    fread(&prox, sizeof(long int), sizeof(long int), f);
    int i = 0;
    while((i < nch)&&(info != ch[i])) i++;
    
    while(i < nch-1){
      ch[i] = ch[i + 1];
      pizza[i] = pizza[i + 1];
    }
    ch[nch-1] = INT_MIN;
    nch--;
    
  }


}*/


// ------------------------------------------------------------------------------------------------------------------------------

/*
ESCREVE DADOS: 

- NUMERO DE CHAVES
- CHAVES
- ENDEREÇO DAS PIZZAS
- POSIÇÃO DA PROXIMA FOLHA

*/
int escreveDados(TABM *a, int T)
{
  if(!a) return -1;
  FILE *arquivo = fopen("dados.dat", "rb+");
  if(!arquivo) exit(1);
  fseek(arquivo, 0, SEEK_END);
  long int endD = a->posMeu = ftell(arquivo), endP;
  fwrite(&a->nchaves, sizeof(int), 1, arquivo); //NUMERO DE CHAVE
  int i;
  fseek(arquivo, 0, SEEK_END);
  for(i = 0; i<T*2-1; i++){ //CHAVE = T*2-1
    fwrite(&a->chave[i], sizeof(int), 1, arquivo);
  }
  FILE *arqPizza = fopen("pizza.dat", "rb+");
  fseek(arqPizza, 0, SEEK_END);
  fseek(arquivo, 0, SEEK_END);
  for(i = 0; i<a->nchaves; i++) //ENDEREÇO DAS PIZZAS
  {
    endP = ftell(arqPizza);
    salva_pizza(a->pizzas[i], arqPizza);
    fwrite(&endP, sizeof(long int), 1, arquivo);
    fseek(arqPizza, 0, SEEK_END);
  }
  fseek(arquivo, 0, SEEK_END);
  for(i = a->nchaves; i<T*2-1; i++) //SE O NUMERO DE PIZZAS FOR MENOR QUE T*2-1
  {
    long int aux = -1;
    fwrite(&aux, sizeof(long int), 1, arquivo);
  }
  fseek(arquivo, 0, SEEK_END);
  fwrite(&a->posProx, sizeof(long int), 1, arquivo); //POSIÇÃO DA PROXIMA FOLHA
  fclose(arqPizza);
  fclose(arquivo);
  TABM * b = a->ant;
  if(b) b->posProx = endD;
  b = a->prox;
  if(b) b->posAnt = endD;
  return 1;
}

/*
ESCREVE INDICE: 

- POSIÇÃO DOS FILHOS
- NÚMERO DE CHAVES
- POSIÇÃO DOS PAIS
- SE FILHO É FOLHA
- CHAVES

*/

long int escreveIndice(TABM *a, int T, int j)
{

  long int TAMINDICE = ((sizeof(int)*T*2-1) + (sizeof(int)*2) + (sizeof(long int)*T*2) + (sizeof(long int)));
  if(!a) return -1;

  FILE *arquivo = fopen("indice.dat", "rb+");
  if(!arquivo) exit(1);
  if(j == 0) fseek(arquivo, 0, SEEK_SET);
  else fseek(arquivo, (vez*TAMINDICE), SEEK_END);

  int i; long int aux2, aux = -1000000, end = ftell(arquivo);
  long int endI = a->posMeu = ftell(arquivo);

  for(i = 0; i<a->nchaves+1; i++)
  {
    if(a->filho[i]->folha){
      aux2 = a->filho[i]->posMeu;
      a->posFilho[i] = aux2;
      fwrite(&aux2, sizeof(long int), 1, arquivo); //POSIÇÃO DOS FILHOS
    }
    else{
      vez++;
      aux2 = escreveIndice(a->filho[i], T, 1);
      fwrite(&aux2, sizeof(long int), 1, arquivo);
    }
  }
  for(i = a->nchaves+1; i<T*2; i++)
  {
    aux2 = -1;
    fwrite(&aux2, sizeof(long int), 1, arquivo); //POSIÇÃO DE FILHOS QUE NÃO EXISTE
  }

  fwrite(&a->nchaves, sizeof(int), 1, arquivo); // NUMERO DE CHAVE

  if(a->pai)fwrite(&a->pai->posMeu, sizeof(long int), 1, arquivo);   // POSIÇÃO DO PAI
  else fwrite(&aux, sizeof(long int), 1, arquivo); 

  if(a->filho[0]->folha) i = 1;
  else i = 0;
  fwrite(&i, sizeof(int), 1, arquivo); //SE FILHO É FOLHA

  for(i = 0; i<T*2-1; i++)
  {
    fwrite(&a->chave[i], sizeof(int), 1, arquivo); // CHAVES
  }

  fclose(arquivo);

  return end;

}

// ESCREVER TODAS AS FOLHS DA DIREITA PARA ESQUERDA
int escreveFolhas(TABM *a, int t)
{
  if(!a) return -1;
  int i;
  while(!a->folha)
  {
    i = a->nchaves;
    a = a->filho[i];
  }
  while(a)
  {
    i = escreveDados(a, t);
    if(i != 1) return -1;
    a = a->ant;
  }
  return 1;

}

//ESCREVE A ARVORE TODA
long int escreveArvore(TABM *a, int t)
{
  vez = 1;
  int i = escreveFolhas(a, t);
  if(i == -1) return -1;
  return escreveIndice(a, t, 0);
}

//ABRE OS ARQUIVOS.
void abreAquivos()
{
  FILE *dados = fopen("dados.dat", "wb+");
  FILE *indice = fopen("indice.dat", "wb+");
  FILE *pizza = fopen("pizza.dat", "wb+");
  fclose(dados);
  fclose(indice);
  fclose(pizza);
}

//FORMA CORRETA DE LER O INDICE
void lerIndice(FILE *arquivo, int t)
{
  int nChave, filhoFolha, chave[t*2-1];
  long int posFilho[t*2-1], proFolha, posPai; 

  int i;

  for(i = 0; i<t*2; i++)
  {
      fread(&posFilho[i], sizeof(long int), 1, arquivo); //POSIÇÃO DOS FILHOS
  }

  fread(&nChave, sizeof(int), 1, arquivo); // NUMERO DE CHAVE
  fread(&posPai, sizeof(long int), 1, arquivo);   // POSIÇÃO DO PAI

  fread(&filhoFolha, sizeof(int), 1, arquivo); //SE FILHO É FOLHA
  for(i = 0; i<t*2-1; i++)
  {
      fread(&chave[i], sizeof(int), 1, arquivo); // CHAVES
  }

  printf("\n----------------- INDICE -------------------\nNUMERO DE CHAVES:  %d\n",nChave );
  printf("POSIÇÃO DO PAI: %ld\n",posPai );
  printf("SE FILHO É FOLHA? %d\nCHAVES: ",filhoFolha);
  for(i = 0; i<t*2-1; i++)
      printf("%d ",chave[i]);
  printf("\nPOSIÇÃO DOS FILHOS: ");
  for (i = 0; i < t*2; i++)
      printf("%ld ", posFilho[i]);
  printf("\n");
}

//FORMA CORRETA DE LER OS DADOS
void lerDados(FILE *arquivo, int t)
{
  int nChaveD, chaveD[t*2-1];
  long int endPizza[t*2-1], proxFolha; 

  int i;

  fread(&nChaveD, sizeof(int), 1, arquivo);//NUMERO DE CAHVE
  for(i = 0; i<t*2-1; i++){ //CHAVE = T*2-1
      fread(&chaveD[i], sizeof(int), 1, arquivo);
  }
  for(i = 0; i<t*2-1; i++) //ENDEREÇO DAS PIZZAS
  {
      fread(&endPizza[i], sizeof(long int), 1, arquivo);
  }
  fread(&proxFolha, sizeof(long int), 1, arquivo); //POSIÇÃO DA PROXIMA FOLHA
  fclose(arquivo);

  printf("\n---------- DADOS ---------\nNUMERO DE CHAVES:  %d\n",nChaveD );
  printf("POSIÇÃO DO PAI: %ld\nCHAVES: ",proxFolha );
  for(i = 0; i<t*2-1; i++)
      printf("%d ",chaveD[i]);
  printf("\nPOSIÇÃO DAS PIZZAS: ");
  for (i = 0; i < t*2-1; i++)
      printf("%ld ", endPizza[i]);
  printf("\n");
}

//FORMA DE LER A PIZZA
int lerPizzaArquivo(FILE *arquivo)
{
  printf("\n---------------- PIZZA COD: 146 -----------------\n");
  TPizza *pizza = le_pizza(arquivo);
  imprime_pizza(pizza);
  printf("\n");
}

long int BuscaDados(long int end, int codigo, int t)
{
    FILE *arquivo = fopen("dados.dat", "rb");
    fseek(arquivo, end, SEEK_SET);

    int nChaveD, chaveD[t*2-1], i;
    long int endPizza[t*2-1], proxFolha; 

    fread(&nChaveD, sizeof(int), 1, arquivo);//NUMERO DE CHAVE
    for(i = 0; i<t*2-1; i++){ //CHAVE = T*2-1
      fread(&chaveD[i], sizeof(int), 1, arquivo);
    }
    for(i = 0; i<t*2-1; i++) //ENDEREÇO DAS PIZZAS
    {
        fread(&endPizza[i], sizeof(long int), 1, arquivo);
    }
    fread(&proxFolha, sizeof(long int), 1, arquivo); //POSIÇÃO DA PROXIMA FOLHA
    fclose(arquivo);

    for(i = 0; i<nChaveD; i++){
      if(chaveD[i] == codigo){
        return endPizza[i];
      }
    }
    return -1;
}

long int BuscaArquivo(long int end, int codigo, int t, FILE *arquivo)
{
    //COLOCA A POSIÇÃO QUE DESEJA
    fseek(arquivo, end, SEEK_SET);
    int i;

    int nChave, filhoFolha, chave[t*2-1];
    long int posFilho[t*2-1], proxFolha, posPai; 

    for(i = 0; i<t*2; i++)
    {
        fread(&posFilho[i], sizeof(long int), 1, arquivo); //POSIÇÃO DOS FILHOS
    }

    fread(&nChave, sizeof(int), 1, arquivo); // NUMERO DE CHAVE
    fread(&posPai, sizeof(long int), 1, arquivo);   // POSIÇÃO DO PAI

    fread(&filhoFolha, sizeof(int), 1, arquivo); //SE FILHO É FOLHA
    for(i = 0; i<t*2-1; i++)
    {
        fread(&chave[i], sizeof(int), 1, arquivo); // CHAVES
    }

    for(i = 0; i<nChave; i++)
    {
      if((codigo < chave[i]) && (!filhoFolha)){
        return BuscaArquivo(posFilho[i], codigo, t, arquivo);
      }
      if((codigo < chave[i]) && (filhoFolha)){
        return BuscaDados(posFilho[i], codigo, t);
      }
      if((i == nChave-1)&&(codigo >= chave[i]))
      {
        if(!filhoFolha)
          return BuscaArquivo(posFilho[i+1], codigo, t, arquivo);
        else return BuscaDados(posFilho[i+1], codigo, t);
      }
    }
    
    return -1;
}

void listaCategoria(char *nome, int t)
{
  long int TAMDADOS = ((sizeof(int)*t*2) + (sizeof(long int)*t*2));
  FILE *dados = fopen("dados.dat", "rb");
  FILE *pizza = fopen("pizza.dat", "rb");
  fseek(dados, 0, SEEK_END);
  long int fim = ftell(dados) - TAMDADOS;
  fseek(dados, fim, SEEK_SET);

  printf("\n");

  while(1)
  {
    int nChaveD, chaveD[t*2-1], i;
    long int endPizza[t*2-1], proxFolha; 

    fread(&nChaveD, sizeof(int), 1, dados);//NUMERO DE CAHVE
    for(i = 0; i<t*2-1; i++){ //CHAVE = T*2-1
        fread(&chaveD[i], sizeof(int), 1, dados);
    }
    for(i = 0; i<t*2-1; i++) //ENDEREÇO DAS PIZZAS
    {
        fread(&endPizza[i], sizeof(long int), 1, dados);
    }
    fread(&proxFolha, sizeof(long int), 1, dados); //POSIÇÃO DA PROXIMA FOLHA

    for(i = 0; i<nChaveD; i++)
    {
      fseek(pizza, endPizza[i], SEEK_SET);
      TPizza *comp = le_pizza(pizza);
      if(strcmp(comp->descricao, nome) == 0){
        imprime_pizza(comp);
      }
    }
    if(proxFolha < 0) break;
    fseek(dados, proxFolha, SEEK_SET);
  }
}

//------------------------------------------------- INSERE EM ARQUIVO --------------------------------------------


/*

long int divisao_indice(long int end, int t, int i){
  
  //VARIAVEIS INDICE Z
  int ZnChave, ZfilhoFolha, Zchave[t*2-1];
  long int ZposFilho[t*2-1], ZposPai;

  //VAREIAVEIS INDICE X
  int XnChave, XfilhoFolha, Xchave[t*2-1];
  long int XposFilho[t*2-1], XposPai; 

  //VARIAVEIS INDICE Y 
  int YnChave, YfilhoFolha, Ychave[t*2-1];
  long int YposFilho[t*2-1], YposPai; 

  //LER OS DADOS DO Y
  FILE *indice = fopen("indice.dat", "rb+");
  //LER Y
  fseek(indice,end, SEEK_SET);
  int m;
  for(m = 0; m<t*2; m++)
  {
    fread(&YposFilho[m], sizeof(long int), 1, indice); //POSIÇÃO DOS FILHOS
  }

  fread(&YnChave, sizeof(int), 1, indice); // NUMERO DE CHAVE
  fread(&YposPai, sizeof(long int), 1, indice);   // POSIÇÃO DO PAI

  fread(&YfilhoFolha, sizeof(int), 1, indice); //SE FILHO É FOLHA
  for(m = 0; m<t*2-1; m++)
  {
    fread(&Ychave[i], sizeof(int), 1, indice); // CHAVES
  }

  XnChave = 0;
  XfilhoFolha = 0;

  int j;
  if(YposPai < -100)
  {
    ZnChave = t-1;
    for(j=0;j<t-1;j++)
      Zchave[j] = Ychave[j+t];
    for(j=0;j<t;j++){
      ZposFilho[j] = YposFilho[j+t];
      YposFilho[j+t] = -1;
    }

    YnChave = t-1;
    
    //----------------------------------------------------------
    Xchave[i-1] = Ychave[t-1];
    XnChave += 1;

    YposPai = 0;
    ZposPai = 0;

    fseek(indice, 0, SEEK_END);
    long int endY = ftell(indice), endZ;
    //ESCREVER Y
    for(m = 0; m<t*2; m++)
    {
      fwrite(&YposFilho[m], sizeof(long int), 1, indice); //POSIÇÃO DOS FILHOS
    }

    fwrite(&YnChave, sizeof(int), 1, indice); // NUMERO DE CHAVE
    fwrite(&YposPai, sizeof(long int), 1, indice);   // POSIÇÃO DO PAI

    fwrite(&YfilhoFolha, sizeof(int), 1, indice); //SE FILHO É FOLHA
    for(m = 0; m<t*2-1; m++)
    {
      fwrite(&Ychave[m], sizeof(int), 1, indice); // CHAVES
    }

    fseek(indice, 0, SEEK_END);
    endZ = ftell(indice);
    //ESCREVER Z
    for(m = 0; m<t*2; m++)
    {
      fwrite(&ZposFilho[m], sizeof(long int), 1, indice); //POSIÇÃO DOS FILHOS
    }

    fwrite(&ZnChave, sizeof(int), 1, indice); // NUMERO DE CHAVE
    fwrite(&ZposPai, sizeof(long int), 1, indice);   // POSIÇÃO DO PAI

    fwrite(&ZfilhoFolha, sizeof(int), 1, indice); //SE FILHO É FOLHA
    for(m = 0; m<t*2-1; m++)
    {
      fwrite(&Zchave[m], sizeof(int), 1, indice); // CHAVES
    }

    rewind(indice);

    XposPai = -1;
    XposFilho[0] = endY;
    XposFilho[1] = endZ;
    //Escrever X
    for(i = 0; i<t*2; i++)
    {
      fwrite(&XposFilho[i], sizeof(long int), 1, indice); //POSIÇÃO DOS FILHOS
    }

    fwrite(&XnChave, sizeof(int), 1, indice); // NUMERO DE CHAVE
    fwrite(&XposPai, sizeof(long int), 1, indice);   // POSIÇÃO DO PAI

    fwrite(&XfilhoFolha, sizeof(int), 1, indice); //SE FILHO É FOLHA
    for(i = 0; i<t*2-1; i++)
    {
      fwrite(&Xchave[i], sizeof(int), 1, indice); // CHAVES
    }
    return 0;

  }

  else{

    fseek(indice, YposPai, SEEK_SET);
    //LER Z
    for(m = 0; m<t*2; m++)
    {
      fread(&ZposFilho[m], sizeof(long int), 1, indice); //POSIÇÃO DOS FILHOS
    }

    fread(&ZnChave, sizeof(int), 1, indice); // NUMERO DE CHAVE
    fread(&ZposPai, sizeof(long int), 1, indice);   // POSIÇÃO DO PAI

    fread(&ZfilhoFolha, sizeof(int), 1, indice); //SE FILHO É FOLHA
    for(m = 0; m<t*2-1; m++)
    {
      fread(&Zchave[m], sizeof(int), 1, indice); // CHAVES
    }

    XposPai = YposPai;

    for(j=0;j<t-1;j++) 
    {
      Xchave[j] = Ychave[j+t];
      XnChave += 1;
    }
    for(j=0;j<t;j++){
      XposFilho[j] = YposFilho[j+t];
      YposFilho[j+t] = -1;
    }

    YnChave -= 1;
    int verf = -1, local = 0;

    for(j = 0; j<ZnChave; j++)
    {
      if(Zchave[j] < Ychave[t-1])
      {
        verf = 1; local = j;
        for(int k =ZnChave; k>j; k--)
          Zchave[k] = Zchave[k-1];
        for(int k = ZnChave+1; k>j+1; k--)
          ZposFilho[k] = ZposFilho[k-1];
      }
    }
    if(verf == 0)
    {
      local = ZnChave;
    }
    Zchave[local] = Ychave[t-1];
    YnChave -= 1;

    fseek(indice, 0, SEEK_END);
    long int endX = ftell(indice);
    //ESCREVER X
    for(i = 0; i<t*2; i++)
    {
      fwrite(&XposFilho[i], sizeof(long int), 1, indice); //POSIÇÃO DOS FILHOS
    }

    fwrite(&XnChave, sizeof(int), 1, indice); // NUMERO DE CHAVE
    fwrite(&XposPai, sizeof(long int), 1, indice);   // POSIÇÃO DO PAI

    fwrite(&XfilhoFolha, sizeof(int), 1, indice); //SE FILHO É FOLHA
    for(i = 0; i<t*2-1; i++)
    {
      fwrite(&Xchave[i], sizeof(int), 1, indice); // CHAVES
    }

    fseek(indice, end, SEEK_SET);
    //ESCREVER Y
    for(m = 0; m<t*2; m++)
    {
      fwrite(&YposFilho[m], sizeof(long int), 1, indice); //POSIÇÃO DOS FILHOS
    }

    fwrite(&YnChave, sizeof(int), 1, indice); // NUMERO DE CHAVE
    fwrite(&YposPai, sizeof(long int), 1, indice);   // POSIÇÃO DO PAI

    fwrite(&YfilhoFolha, sizeof(int), 1, indice); //SE FILHO É FOLHA
    for(m = 0; m<t*2-1; m++)
    {
      fwrite(&Ychave[m], sizeof(int), 1, indice); // CHAVES
    }

    fseek(indice, YposPai, SEEK_SET);
    ZposFilho[local+1] = endX;
    //ESCREVER Z
    for(m = 0; m<t*2; m++)
    {
      fwrite(&ZposFilho[m], sizeof(long int), 1, indice); //POSIÇÃO DOS FILHOS
    }

    fwrite(&ZnChave, sizeof(int), 1, indice); // NUMERO DE CHAVE
    fwrite(&ZposPai, sizeof(long int), 1, indice);   // POSIÇÃO DO PAI

    fwrite(&ZfilhoFolha, sizeof(int), 1, indice); //SE FILHO É FOLHA
    for(m = 0; m<t*2-1; m++)
    {
      fwrite(&Zchave[m], sizeof(int), 1, indice); // CHAVES
    }

    fclose(indice);

    return YposPai;

  }

  return LONG_MIN;
  
}


long int divisao_dados(long int endY, long int endP, int i, int chave, int t){

  //VARIAVES DADOS Z
  int ZnChave, Zchave[t*2-1];
  long int ZposPizza[t*2-1], ZproxFolha; 

  //VARIAVES DADOS Y
  int YnChave, Ychave[t*2-1];
  long int YposPizza[t*2-1], YproxFolha; 

  //VARIAVES INDICE X
  int XnChave, XfilhoFolha, Xchave[t*2-1];
  long int XposFilho[t*2-1], XposPai;

  //LER Y
  FILE *dados = fopen("dados.dat", "rb");
  fseek(dados, endY, SEEK_SET);
  fread(&YnChave, sizeof(int), 1, dados);//NUMERO DE CAHVE
  for(i = 0; i<t*2-1; i++){ //CHAVE = T*2-1
    fread(&Ychave[i], sizeof(int), 1, dados);
  }
  for(i = 0; i<t*2-1; i++) //ENDEREÇO DAS PIZZAS
  {
    fread(&YposPizza[i], sizeof(long int), 1, dados);
  }
  fread(&YproxFolha, sizeof(long int), 1, dados); //POSIÇÃO DA PROXIMA FOLHA

  ZproxFolha = YproxFolha;

  int j;

  ZnChave = t; 
  for(j=0;j < t;j++){
    ZposPizza[j] = YposPizza[j+t-1];
    Zchave[j] = YposPizza[j+t-1];
  }

  YnChave = t-1;

  int cod = Ychave[t-1];
  FILE *indice = fopen("indice.dat", "rb+");
  fseek(indice, endP, SEEK_SET);
  //LER X
  int m;

  for(m = 0; m<t*2; m++)
  {
    fread(&XposFilho[m], sizeof(long int), 1, indice); //POSIÇÃO DOS FILHOS
  }

  fread(&XnChave, sizeof(int), 1, indice); // NUMERO DE CHAVE
  fread(&XposPai, sizeof(long int), 1, indice);   // POSIÇÃO DO PAI

  fread(&XfilhoFolha, sizeof(int), 1, indice); //SE FILHO É FOLHA
  for(m = 0; m<t*2-1; m++)
  {
    fread(&Xchave[m], sizeof(int), 1, indice); // CHAVES
  }

  int verf = 0, local = 0;

  for(j = 0; j<XnChave; j++)
  {
    if(Xchave[j] < cod)
    {
      verf = 1; local = j;
      for(int k = XnChave; k>j; k--)
        Xchave[k] = Xchave[k-1];
      for(int k = XnChave+1; k>j+1; k--)
        XposFilho[k] = XposFilho[k-1];
    }
  }
  if(verf == 0)
  {
    for(int k = XnChave; k>0; k--)
      Xchave[k] = Xchave[k-1];
    for(int k = ZnChave+1; k>j+1; k--)
      XposFilho[k] = XposFilho[k-1];
  }
  Xchave[local] = cod;
  XnChave += 1;

  fseek(dados, 0, SEEK_END);
  long int endZ = ftell(dados);
  //ESCREVER Z DADOS
  fwrite(&ZnChave, sizeof(int), 1, dados); //NUMERO DE CAHVE
  for(i = 0; i<t*2-1; i++){ //CHAVE = T*2-1
    fwrite(&Zchave[i], sizeof(int), 1, dados);
  } 
  for(i = 0; i<t*2-1; i++) //ENDEREÇO DAS PIZZAS
  {
    fwrite(&ZposPizza, sizeof(long int), 1, dados);
  }
  fwrite(&ZproxFolha, sizeof(long int), 1, dados); //POSIÇÃO DA PROXIMA FOLHA
  
  fseek(dados, endY, SEEK_END);
  YproxFolha = endZ;
  //ESCREVER Y DADOS
  fwrite(&YnChave, sizeof(int), 1, dados); //NUMERO DE CAHVE
  for(i = 0; i<t*2-1; i++){ //CHAVE = T*2-1
    fwrite(&Ychave[i], sizeof(int), 1, dados);
  } 
  for(i = 0; i<t*2-1; i++) //ENDEREÇO DAS PIZZAS
  {
    fwrite(&YposPizza, sizeof(long int), 1, dados);
  }
  fwrite(&YproxFolha, sizeof(long int), 1, dados); //POSIÇÃO DA PROXIMA FOLHA
  fclose(dados);

  fseek(indice, endP, SEEK_SET);
  XposFilho[local+1] = endZ;
  //ESCREVER X INDICE
  for(i = 0; i<t*2; i++)
  {
    fwrite(&XposFilho[i], sizeof(long int), 1, indice); //POSIÇÃO DOS FILHOS
  }

  fwrite(&XnChave, sizeof(int), 1, indice); // NUMERO DE CHAVE
  fwrite(&XposPai, sizeof(long int), 1, indice);   // POSIÇÃO DO PAI

  fwrite(&XfilhoFolha, sizeof(int), 1, indice); //SE FILHO É FOLHA
  for(i = 0; i<t*2-1; i++)
  {
    fwrite(&Xchave[i], sizeof(int), 1, indice); // CHAVES
  }
  fclose(indice);

  if(chave > cod) return endZ;
  if(chave < cod) return endY;
}


long int insere_nao_completo_dados(long int pos, TPizza *p, int t){

  //VARIAVEL DE X DADO
  int XnChave, Xchave[t*2-1];
  long int XposPizza[t*2-1], XproxFolha;

  FILE *dados = fopen("dados.dat", "rb+");
  fseek(dados, pos, SEEK_SET);
  //LER X DADOS
  int i;
  fwrite(&XnChave, sizeof(int), 1, dados); //NUMERO DE CAHVE
  for(i = 0; i<t*2-1; i++){ //CHAVE = T*2-1
    fwrite(&Xchave[i], sizeof(int), 1, dados);
  }
  for(i = 0; i<t*2-1; i++) //ENDEREÇO DAS PIZZAS
  {
    fwrite(&XposPizza, sizeof(long int), 1, dados);
  }
  fwrite(&XproxFolha, sizeof(long int), 1, dados); //POSIÇÃO DA PROXIMA FOLHA

  i = XnChave-1;
  while((i>=0) && (p->cod < Xchave[i])){
    XposPizza[i+1] = XposPizza[i];
    Xchave[i+1] = Xchave[i];
    i--;
  }

  fseek(pizza, 0 , SEEK_END); 
  int long endPizza = ftell(pizza);
  fwrite(&p, sizeof(TPizza), 1, pizza);
  fclose(pizza);

  XposPizza[i+1] = endPizza;
  XnChave++;

  fseek(dados, pos, SEEK_SET);
  fwrite(&XnChave, sizeof(int), 1, dados); //NUMERO DE CAHVE
  for(i = 0; i<t*2-1; i++){ //CHAVE = T*2-1
    fwrite(&Xchave[i], sizeof(int), 1, dados);
  } 
  for(i = 0; i<t*2-1; i++) //ENDEREÇO DAS PIZZAS
  {
    fwrite(&XposPizza, sizeof(long int), 1, dados);
  }
  fwrite(&XproxFolha, sizeof(long int), 1, dados);
 
  fclose(dados);

  return endPizza;
  
}

long int busca_insere_nao_completo(long int pos, TPizza *p, int t)
{
  //VAREIAVEIS INDICE X
  int XnChave, XfilhoFolha, Xchave[t*2-1], m;
  long int XposFilho[t*2-1], XposPai;

  FILE *indice = fopen("indice.dat", "rb+");
  fseek(indice, pos, SEEK_SET);

  //LER INDICE X
  for(m = 0; m<t*2; m++)
  {
    fread(&XposFilho[m], sizeof(long int), 1, indice); //POSIÇÃO DOS FILHOS
  }

  fread(&XnChave, sizeof(int), 1, indice); // NUMERO DE CHAVE
  fread(&XposPai, sizeof(long int), 1, indice);   // POSIÇÃO DO PAI

  fread(&XfilhoFolha, sizeof(int), 1, indice); //SE FILHO É FOLHA
  for(m = 0; m<t*2-1; m++)
  {
    fread(&Xchave[m], sizeof(int), 1, indice); // CHAVES
  }

  int i = XnChave - 1;

  while((i>=0) && (p->cod < Xchave[i])) 
  {
    i--;
  }

  i++;

  if(XfilhoFolha)
  {;
    FILE *dados = fopen("dados.dat", "rb+");
    fseek(dados, XposFilho[i], SEEK_SET);
    int nChave, chave[t*2-1];
    long int posPizza[t*2-1], proxFolha;
    fwrite(&nChave, sizeof(int), 1, dados); //NUMERO DE CAHVE
    for(i = 0; i<t*2-1; i++){ //CHAVE = T*2-1
      fwrite(&chave[i], sizeof(int), 1, dados);
    }
    for(i = 0; i<t*2-1; i++) //ENDEREÇO DAS PIZZAS
    {
      fwrite(&posPizza, sizeof(long int), 1, dados);
    }
    fwrite(&proxFolha, sizeof(long int), 1, dados); //POSIÇÃO DA PROXIMA FOLHA
    
    if(nChave == ((2*t)-1)) {
      long int c = divisao_dados(XposFilho[i], pos,(i+1), p->cod, t);

      fclose(pizza);
      fclose(indice);
      fclose(dados);

      return insere_nao_completo_dados(c, p, t);
    }
    else insere_nao_completo_dados(XposFilho[i], p, t);
  }
  else{
    fseek(indice, XposFilho[i], SEEK_SET);
    int YnChave, YfilhoFolha, Ychave[t*2-1];
    long int YposFilho[t*2-1], YposPai; 
    int m;
    for(m = 0; m<t*2; m++)
    {
      fread(&YposFilho[m], sizeof(long int), 1, indice); //POSIÇÃO DOS FILHOS
    }

    fread(&YnChave, sizeof(int), 1, indice); // NUMERO DE CHAVE
    fread(&YposPai, sizeof(long int), 1, indice);   // POSIÇÃO DO PAI

    fread(&YfilhoFolha, sizeof(int), 1, indice); //SE FILHO É FOLHA
    for(m = 0; m<t*2-1; m++)
    {
      fread(&Ychave[m], sizeof(int), 1, indice); // CHAVES
    }
    if(YnChave == ((2*t)-1)){
      long int c = divisao_indice(XposFilho[i], t, 1);
      busca_insere_nao_completo(c, p, t);
    }
    return busca_insere_nao_completo(XposFilho[i], p, t);
  }
}

long int insere_no_arquivo(int t, TPizza *p){
  //if(busca(T, mat)) return T;

  FILE *indice = fopen("indice.dat", "rb+");
  rewind(indice);

  int YnChave, YfilhoFolha, Ychave[t*2-1];
  long int YposFilho[t*2-1], YposPai; 
  int m;
  for(m = 0; m<t*2; m++)
  {
    fread(&YposFilho[m], sizeof(long int), 1, indice); //POSIÇÃO DOS FILHOS
  }

  fread(&YnChave, sizeof(int), 1, indice); // NUMERO DE CHAVE
  fread(&YposPai, sizeof(long int), 1, indice);   // POSIÇÃO DO PAI

  fread(&YfilhoFolha, sizeof(int), 1, indice); //SE FILHO É FOLHA
  for(m = 0; m<t*2-1; m++)
  {
    fread(&Ychave[m], sizeof(int), 1, indice); // CHAVES
  }
  fclose(indice);

  if(YnChave == (2*t)-1){
    int long x = divisao_indice(0, T, 1);   
    return busca_insere_nao_completo(0, p, T);
  }
  return busca_insere_nao_completo(0, p, T);
}


*/
