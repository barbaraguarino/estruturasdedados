**Trabalho Computacional de Estrutura de Dados e seus algoritmos**

Alunos: Barbara 

Professora: Isabel Rosseti

---

## O que se espera?

*Manipulação de árvore B+ em arquivos por meio do gerenciamento de um catálogo de pizzas*

1. Seu programa deve receber como entrada os seguintes parâmetros: o fator de ramificação (t) da árvore B+ e um catálogo inicial no formato previamente especificado. 
2. Além disso, sua implementação deve ser capaz de distinguir entre as informações principais consideradas chaves primárias (nesse caso, o código da pizza - int) e as informações subordinadas (nome da pizza - string de 50 caracteres, nome da categoria - string de 20 caracteres e preço - float). 
3. **A árvore B+ deve ser armazenada em disco.** 
4. As seguintes operações devem ser implementadas nesse trabalho:
	1. inserção e remoção de nós da árvore B+;
	2. busca das informações subordinadas, dada a chave primária;
	3. alteração SOMENTE das informações subordinadas;
	4. busca de todas as pizzas de uma determinada categoria; e
	5. remoção de todas as pizzas de uma determinada categoria. 

---

**Informações importantes:**

1. [Exemplo](http://www.ic.uff.br/~rosseti/EDA/2019-1/TC.zip) de arquivo de entrada no modo binário (que deve ser seguido pelo seu programa), bem como da estrutura usada para criá-lo, gentilmente cedida pela Professora Vanessa Braganholo. O uso das funções TPizza **- *le_pizza(FILE *in); e void imprime_pizza(TPizza *p); - ** podem ser usadas para verificar a leitura correta do arquivo binário. 
2. A **impressão** do arquivo binário **deve ser a mesma da descrita** [aqui](http://www.ic.uff.br/~rosseti/EDA/2019-1/pizza.txt);

---
  
**Datas e grupos**

1. grupo de no mínimo dois e de no máximo três discentes;
2. data limite de entrega: 01/07/2019 às 23:59h; e semana de apresentação: de 02 até 05/07/2019. 

**Mais Informações**

Podem ser encontradas no [site](http://www.ic.uff.br/~rosseti/EDA/2019-1/index.html) da professora

---

