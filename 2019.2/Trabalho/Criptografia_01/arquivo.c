#include <string.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct pizza {
	char letra[3];
	int c1;
	int c2;
	float frequencia;
}TDados;

void imprime_dados(TDados *p)
{
	printf("%s, %d, %d, %f\n",p->letra, p->c1, p->c2, p->frequencia);
}


TDados *dado(char *letra, int c1, int c2, float f)
{
	TDados *p = (TDados *) malloc(sizeof(TDados));
	if (p) memset(p, 0, sizeof(TDados));
	strcpy(p->letra, letra);
	p->c1 = c1;
	p->c2 = c2;
	p->frequencia = f;
	return p;
}


void salva_dados(TDados *p, FILE *out)
{
	fwrite(p->letra, sizeof(char), sizeof(p->letra), out);
	fwrite(&p->c1, sizeof(int), 1, out);
	fwrite(&p->c2, sizeof(int), 1, out);
	fwrite(&p->frequencia, sizeof(float), 1, out);
}


TDados *le_dados(FILE *in)
{
	TDados *p = (TDados *) malloc(sizeof(TDados));
	if (0 >= fread(p->letra, sizeof(char), sizeof(p->letra), in)) {
		free(p);
		return NULL;
	}
	fread(&p->c1, sizeof(int), 1, in);
	fread(&p->c2, sizeof(int), 1, in);
	fread(&p->frequencia, sizeof(float), 1, in);
	return p;
}

TDados *le_dadosTXT(FILE *in){
	TDados *p = (TDados *) malloc(sizeof(TDados));
	if (0 > fscanf(in, "%s %d %d %f", p->letra, &p->c1, &p->c2, &p->frequencia)) {
		free(p);
		return NULL;
	}else return p;
}

int long tamanho_dados_bytes()
{
	return sizeof(char) * 3 + // nome
		sizeof(int) + // categoria
		sizeof(int) +
		sizeof(float); // preço
}

/*

void main(){

	FILE *entrada, *saida;
	entrada = fopen("dados.txt", "r");
	saida = fopen("dados.b", "rb+");

	if(!entrada || !saida) exit(1);


	//Montar o arquivo binario
	do{
		TDados *e = le_dadosTXT(entrada);
		if(!e) break;
		salva_dados(e, saida);
	}while(1);



	fclose(entrada);
	fclose(saida);

}

*/