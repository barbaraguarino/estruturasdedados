**Estrutura de Dados e Seus Algoritmos - 2019.1**

Professora: Rosseti

Curso ministrado em 2019.1

---

## Site
---

**Informações importantes:**

Podem ser encontradas no [site](http://www.ic.uff.br/~rosseti/EDA/2019-1/index.html) da professora

---

## Aulas ministradas

**26/03/2019**

1. Motivação para o uso de árvores e de árvores binárias;
2. Implementação de árvores binárias;
3. Operações básicas: (a) inicialização e (b) criação de uma árvore, a partir dos seguintes parâmetros de entrada: a informação do nó raiz e as sub-árvores esquerda e direita;
4. Percurso em árvores binárias: pré-ordem (ou profundidade), simétrica, pós-ordem e largura;
5. Operações básicas mostrando as diversas formas de impressão: profundidade (versões recursiva e iterativa, por meio do uso de uma implementação específica de pilha), simétrica (versão recursiva), pós-ordem (versão recursiva) e largura (versão iterativa usando uma implementação específica de fila); e
6. [Exercícios](http://www.ic.uff.br/~rosseti/EDA/2019-1/lista1-EDA-2019-1.pdf).
 
**28/03/2019**

1. Outras operações sobre árvores binárias desenvolvidas em sala: (a) libera todos os nós e (b) busca um elemento na árvore;
2. Exemplo de uso das operações supracitadas: dados um vetor ordenado e seu tamanho, gere uma árvore binária com o mesmo número de filhos nas sub-árvores esquerda e direita, usando a operação de criação;
3. Motivação para o uso de árvores binárias de busca (ABB);
4. Implementação de árvores binárias de busca;
5. Operações básicas (que diferem de árvore binária original): busca e inserção de um elemento; e
6. [Exercícios](http://www.ic.uff.br/~rosseti/EDA/2019-1/lista2-EDA-2019-1.pdf). 
    
**02/04/2019**

1. Alguns exemplos desenvolvidos em sala de aula:
	1. Copiar uma árvore binária - TAB* copia(TAB *a);
	2. Espelhar uma árvore binária - TAB* espelho(TAB *a);
	3. Versões iterativa, usando filas, e recursiva de coloração de árvores binárias balanceadas - void colore(TAB *a); e
	4. Retornar o maior elemento da árvore binária - TAB* maior(TAB *a). 
2. Ideia da operação básica de retirada de um elemento de uma árvore binária de busca. 
    
**04/04/2019**

1. Retirada de um elemento de uma árvore binária de busca;
2. Implementação da função de altura de uma árvore binária;
3. Motivação para o uso de árvore binária de busca AVL;
4. Definição de árvore binária de busca AVL; e
5. Operações básicas: (a) rotação simples a esquerda - RSE e (b) rotação simples a direita - RSD. 
    
**11/04/2019**

1. [Pacote](http://www.ic.uff.br/~rosseti/EDA/2019-1/TAB_TABB.zip) de bibliotecas usadas para a resolução dos seguintes exercícios:
	1. testar se uma árvore é zigue-zague, isto é, todos os nós internos possuem exatamente uma sub-árvore vazia - int zz(TAB *a);
	2. retornar todos os ancestrais de um nó na árvore de busca binária, da raiz da árvore até o elemento passado como parâmetro, usando a biblioteca de [lista encadeada](http://www.ic.uff.br/~rosseti/EDA/2019-1/TLSE.zip) - TLSE* ancestrais(TABB *a, int elem);
	3. verificar se uma árvore é estritamente binária, isto é, os nós dessa árvore possuem ou zero ou dois filhos - int estbin(TAB *a);
	4. testar se duas árvores possuem os mesmos nós - int mn(TAB *a1, TAB *a2); 
2. Operações básicas: (a) rotação dupla esquerda-direita - RED e (b) rotação dupla direita-esquerda - RDE;
3. Apresentação, por meio de exemplos, das operações de inserção e retirada em AVL, utilizando, quando necessário, as operações de rotação supracitadas;
4. Grafos: apresentação de conceitos; e
[Exercícios](http://www.ic.uff.br/~rosseti/EDA/2019-1/lista3-EDA-2019-1.pdf). 
    
**16/04/2019**

1. Algumas definições a respeito de grafos;
2. Representação de grafos por meio de listas encadeadas;
3. Operações em grafos: (a) inicialização, (b) busca de nós e de arestas do grafo, (c) impressão, (d) liberação da estrutura, e (e) inserção de nós e de arestas do grafo; e
4. [Exercícios](http://www.ic.uff.br/~rosseti/EDA/2019-1/lista4-EDA-2019-1.pdf). 
    
**25/04/2019**

1. Retirada de nós em grafos;
2. [Código](http://www.ic.uff.br/~rosseti/EDA/2019-1/avl-esq.c) da árvore binária de busca AVL;
3. Definição de arquivos;
4. Tipos de arquivos: binários e de texto;
5. Funções para manipulação de arquivos em C (biblioteca stdio.h):
	1. *fopen* e *fclose* para abrir e fechar arquivos, respectivamente; e
	2. *fscanf* e *fprintf* para leitura e escrita de informações em arquivos texto, respectivamente. 
6. Exemplo de uso das funções: merge de dois arquivos texto ordenados, gerando um terceiro também ordenado com elementos repetidos. 
    
**30/04/2019**

1. Funções para manipulação de arquivos binários em C (biblioteca stdio.h):
	1. *fread* e *fwrite* para leitura e escrita de informações, respectivamente;
	2. *fseek* para mover a posição de um arquivo binário para uma nova posição específica;
	3. rewind para colocar o cursor no início do arquivo binário; e
	4. *ftell* para obter a posição corrente do arquivo binário. 
2. Exemplos de uso destas funções: criação de arquivos (nos modos binário e texto) ordenados, busca binária e ordenação por seleção em arquivos binários; e
3. Exercícios: reescreva os algoritmos de ordenação e de busca binária desenvolvidos nesta aula, de modo que eles suportem quaisquer tipos de dados. 
    
**02/05/2019**

2. Motivação para classificação (ou ordenação) externa;
3. Etapas da classificação externa: geração de partições ordenadas e intercalação dessas partições;
4. Algoritmos que podem ser usados na etapa de geração de partições classificadas: ou classificação interna, ou seleção com substituição, ou seleção natural;
5. Ideia do algoritmo de classificação interna.
6. Ideia do algoritmo de seleção com substituição;
7. Entendimento do algoritmo de seleção natural; e
8. Desenvolvimento da primeira versão do algoritmo de classificação interna. 
    
**07/05/2019** *(aula no Laboratório 306)*

1. Exercícios sobre as seguintes estruturas de dados:
	1. [grafos](http://www.ic.uff.br/~rosseti/EDA/2019-1/TG.zip):
		1. verificar se o grafo, passado como parâmetro de entrada, possui todos os nós com grau igual a k - int testek(TG *g, int k); e
		2. testar se dois grafos são iguais - int ig(TG *g1, TG *g2).
		3. [Exercício do Instagram](http://www.ic.uff.br/~rosseti/EDA/2019-1/ex_Instagram.pdf), gentilmente cedido pela Professora Vanessa Braganholo. 
	2. arquivos:
		1. escrever o algoritmo de ordenação por bolha - void BolhaBin(char *nomeArq);
		2. desenvolver um procedimento que receba o nome de um arquivo texto e retire deste texto palavras consecutivas repetidas. O seu programa deve retornar, no arquivo de saída, informado como parâmetro dessa função, a resposta desta questão. Por exemplo, se o conteúdo de um arquivo texto for: "Isto e um texto texto repetido repetido repetido . Com as repeticoes repeticoes fica fica sem sem sentido . Sem elas elas elas melhora melhora um um pouco .", a saída do seu programa será: "Isto e um texto repetido . Com as repeticoes fica sem sentido . Sem elas melhora um um pouco ." - void RetRepet(char *ArqEnt, char *ArqSaida);
		3. implementar um algoritmo que receba como parâmetro de entrada, o nome de um arquivo texto, cujo conteúdo são o nome do aluno e as duas notas dos alunos do curso, uma em cada linha, e que ordene o arquivo de saída em ordem crescente pela média do aluno. Isto é, se eu tiver como entrada o arquivo: "P C/10.0/10.0-J J/3.0/4.0-G G/7.0/7.0-A A/0.5/1.5-I I/5.0/6.0", a saída será: "A A/1.0-J J/3.5-I I/5.5-G G/7.0-P C/10.0" - void media(char *ArqEnt, char *ArqSaida); e
		4. escrever um procedimento que receba o nome de um arquivo texto, cujo conteúdo são valores inteiros, e um inteiro N e imprima na tela o número de vezes que N aparece e em quais linhas - void infoN(char *Arq, int N). 
    
**09/05/2019**

1. Códigos: [busca binária](http://www.ic.uff.br/~rosseti/EDA/2019-1/bb.c), e ordenações por [seleção](http://www.ic.uff.br/~rosseti/EDA/2019-1/sel_sort_bin.c) e usando [QuickSort](http://www.ic.uff.br/~rosseti/EDA/2019-1/qs.c);
2. [Pacote](http://www.ic.uff.br/~rosseti/EDA/2019-1/Ger_Part_Classif.zip) de algoritmos de geração de partições classificadas;
3. Dúvidas a respeito de AVL; e
4. Exercício: reescreva o algoritmo de seleção natural (com o reservatório implementado em memórias primária e secundária), sendo o reservatório menor que a memória principal. 
    
**14/05/2019**

1. Segunda etapa da classificação externa: intercalação;
2. Objetivo desta etapa;
3. Intercalação de arquivos sequenciais ordenados: cenário de uso considerando o número de partições n menor que o limite de arquivos abertos simultaneamente em um sistema operacional;
4. Algoritmo básico: O(n) na busca pela menor chave;
5. Ideia do algoritmo básico de intercalação;
6. Otimização do algoritmo básico de intercalação - uso de árvores binárias de vencedores: O(log n) na busca pela menor chave;
7. Ideia do algoritmo de árvores binárias de vencedores;
8. Problema da etapa de intercalação: geração do arquivo final a partir de n arquivos de partições, com a limitação de um sistema operacional manter um número finito de arquivos abertos menor que n;
9. Medida de eficiência: número de passos;
Ideia do algoritmo de intercalação ótima; e
10. Exercícios:
	1. faça o algoritmo de árvores binárias de vencedores usando a estrutura de árvore binária; e
	2. considerando um dos algoritmos de geração de partições classificadas e o de árvores binárias de vencedores (o último dsenvolvido no exercício supracitado), implemente a intercalação ótima, com a limitação de um sistema operacional hipotético manter somente quatro arquivos abertos simultaneamente. 
    
**16/05/2019**

1. Motivação para o uso de árvores de múltiplos caminhos;
2. Introdução a árvores B;
3. Definição de árvores B;
4. Implementação da estrutura de árvore B e das seguintes operações: (a) criação de nó de árvore B e (b) busca de um elemento; e
5. Ideia da operação de inserção. 
    
**23/05/2019**

1. Implementação das operações de impressão e de liberação de árvores B; e
2. Início do desenvolvimento da operação de inserção. 
    
**28/05/2019**

1. Implementação da operação de inserção, bem como das operações de divisão e de inserção em um nó não completo necessárias para o funcionamento dessa codificação;
2. Explicação dos casos necessários (1, 2A, 2B, 2C, 3A e 3B) para o desenvolvimento do algoritmo de remoção em árvores B, ilustrando-os com exemplos; e
3. [Código](http://www.ic.uff.br/~rosseti/EDA/2019-1/arvb.c) da árvore B. 
    
**30/05/2019**

1. Execução de códigos que implementam a [intercalação]() de partições classificadas e a árvore B;
Exercícios de intercalação de partições classificadas:
	1. reorganize o código original de árvores binárias de vencedores a fim de usar a primeira posição da heap;
	2. refaça o algoritmo de árvores binárias de vencedores usando a estrutura de árvore binária no lugar da implementação prévia de heap; e
	3. considerando um dos algoritmos de geração de partições classificadas e o de árvores binárias de vencedores, implemente a intercalação ótima, com a limitação de um sistema operacional hipotético manter somente quatro arquivos abertos simultaneamente. 
2. Motivação para o uso de árvores B+;
3. Definição de árvores B+;
4. Discussão de algumas operações em árvores B+:
	1. Tratamento das divisões de nós requeridas na operação de inserção, quando estão envolvidos somente nós internos (caso idêntico ao da árvore B original), e quando os tipos dos nós são heterogêneos, isto é, nós internos e folhas; e
	2. Soluções encontradas para lidar com os casos 3A e 3B da operação de retirada, quando estão envolvidos somente nós internos (caso idêntico ao da árvore B original), e quando os tipos dos nós são heterogêneos, isto é, nós internos e folhas; e 
5. [Código](http://www.ic.uff.br/~rosseti/EDA/2019-1/arvb_mais.c) da árvore B+, com exceção da operação de retirada; e
6. [Exercícios](http://www.ic.uff.br/~rosseti/EDA/2019-1/lista5-EDA-2019-1.pdf) de árvores B e B+.
7. **Trabalho computacional - manipulação de árvore B+ em arquivos por meio do gerenciamento de um catálogo de pizzas** → seu programa deve receber como entrada os seguintes parâmetros: o fator de ramificação (t) da árvore B+ e um catálogo inicial no formato previamente especificado. Além disso, sua implementação deve ser capaz de distinguir entre as informações principais consideradas chaves primárias (nesse caso, o código da pizza - int) e as informações subordinadas (nome da pizza - string de 50 caracteres, nome da categoria - string de 20 caracteres e preço - float). A árvore B+ deve ser armazenada em disco. As seguintes operações devem ser implementadas nesse trabalho:
	1. inserção e remoção de nós da árvore B+;
	2. busca das informações subordinadas, dada a chave primária;
	3. alteração SOMENTE das informações subordinadas;
	4. busca de todas as pizzas de uma determinada categoria; e
	5. remoção de todas as pizzas de uma determinada categoria. 
8. **Informações importantes:**
	1. [exemplo](http://www.ic.uff.br/~rosseti/EDA/2019-1/TC.zip) de arquivo de entrada no modo binário (que deve ser seguido pelo seu programa), bem como da estrutura usada para criá-lo, gentilmente cedida pela Professora Vanessa Braganholo. O uso das funções TPizza *le_pizza(FILE *in); e void imprime_pizza(TPizza *p); podem ser usadas para verificar a leitura correta do arquivo binário. A impressão do arquivo binário deve ser a mesma da descrita [aqui](http://www.ic.uff.br/~rosseti/EDA/2019-1/pizza.txt);
    
**06/06/2019**

1. Execução de código referente a árvore B+, com exceção da operação de retirada;
2. Exercício: considerando que só existam as chaves primárias, do tipo int, na implementação supracitada, implemente a operação de retirada em árvore B+;
3. Motivação para o uso de tabelas hash (ou tabelas de dispersão);
4. Definição de tabelas hash;
5. Características ideais das funções de hash: ser facilmente computável, ser determinística, seguir a distribuição uniforme e produzir um número baixo de colisões;
6. Exemplo de função de hash - método da divisão;
7. Tipos de tratamento de colisões: (a) por endereçamento aberto, (b) por encadeamento externo e (c) por encadeamento interno;
8. Tentativas de cálculo de novo endereçamento devido as colisões para endereçamento aberto: (a) linear, (b) quadrática e (b) dispersão dupla; e
9. Definição das operações para o tratamento de colisão usando endereçamento aberto (e considerando tentativa linear): (a) inicializa, (b) aloca, (c) insere, (d) busca, (e) retira e (f) libera. 
    
**11/06/2019**

1. Tratamento de colisão usando encadeamento externo para a memória principal;
2. Definição das operações para o tratamento de colisões baseado em encadeamento externo em memória principal: (a) inicializa, (b) aloca, (c) insere, (d) busca, (e) retira e (f) libera;
3. Discussão sobre o tratamento de colisão por encadeamento externo em memória secundária; e
4. Definição das operações para o tratamento de colisões baseado em encadeamento externo em memória secundária: (a) inicializa, (b) aloca e (c) insere. 
    
**13/06/2019**

1. Definição das operações para o tratamento de colisões baseado em encadeamento externo em memória secundária: (a) busca e (b) retira;
2. Ideia do tratamento de colisão usando encadeamento interno para a memória principal; e
    
**18/06/2019**

1. Discussão do tratamento de colisão usando encadeamento interno para a memória principal;
2. Definição das operações (usando tentativa linear): inicializa, aloca, libera, insere, busca e retira;
3. Tratamento de colisão usando encadeamento interno para a memória secundária;
4. Ideias das operações em memória secundária; e
5. Exercício: desenvolver as operações básicas para o tratamento de colisão por encadeamento interno em memória secundária. Este tipo de tratamento é semelhante ao tratamento de colisões baseado em encadeamento externo para o mesmo tipo de memória, juntando, num único arquivo, as informações de hash e de dados.
    
**25/06/2019**

1. [Implementações](http://www.ic.uff.br/~rosseti/EDA/2019-1/hash.zip) de tabelas hash desenvolvidas em sala de aula;
2. Motivação para o uso de listas de prioridades ou heaps binários;
3. Definição das operações de heaps binários de máximo em memória principal:
	1. *int pai (int indice)*;
	2. *int esquerda (int indice)*;
	3. *int direita (int indice)*;
	4. *void max_heapfy (int* heap, int n, int indice)*;
	5. *void build_maxheap (int* heap, int n)*; e
	6. *void heapsort (int* heap, int n)*. 

**27/06/2019**

1. Definição das operações de heaps binários em memória secundária; e
2. [Implementações](http://www.ic.uff.br/~rosseti/EDA/2019-1/Heap.zip) de heaps desenvolvidas em sala de aula. 
    
**02/07/2019** *(aula de dúvidas para a segunda prova no laboratório 306)*

1. Exercícios sobre as seguintes estruturas de dados:
	1. Árvores B e B+:
		1. cópia de uma árvore: TAB* copia (TAB *a);
		2. sucessor de um elemento na árvore. Se o elemento for o maior da estrutura, sua função deve retornar INT_MAX: int suc (TAB *a, int elem);
		3. maior elemento da árvore: TAB* maior(TAB *a);
		4. menor elemento da árvore: TAB* menor(TAB *a);
		5. função que, dadas duas árvores deste tipo, testa se estas árvores são iguais. A função retorna um se elas são iguais e zero, caso contrário. A função deve obedecer ao seguinte protótipo: int igual (TAB* a1, TAB* a2);
		6. função em C que, dada uma árvore qualquer, retire todos os elementos pares da árvore original. A função deve ter o seguinte protótipo: TAB* retira_pares (TAB* arv);
		7. quantidade de nós internos: int ni(TAB *a);
		8. quantidade de nós folha: int nf(TAB *a); 
	2. Tabelas hash:
		1. escreva uma operação de limpeza, em memória secundária, usando tratamento de colisão baseado em encadeamento externo: void limpeza(char *hash, char *dados, char *novodados, int N);
		2. implemente um procedimento que, dados uma tabela hash, uma matrícula e um cr, retire dessa tabela todos os dados com a mesma colisão da matrícula passada como parâmetro de entrada, e que tenham cr menor ou igual ao cr supracitado: void f(char *hash, char *dados, int N, int mat, float cr); 
	3. Heaps: generalize as operações de heaps binários de máximo para heaps ternários de mínimo (int filho (int indice, int pos), onde pos ∈ {1,2,3}, int pai (int indice), void min_heapfy (int* heap, int n, int indice) e void build_minheap (int* heap, int n)), onde os métodos esquerda e direita são as operações filho (pos, 1) e filho(pos, 2), respectivamente. 


