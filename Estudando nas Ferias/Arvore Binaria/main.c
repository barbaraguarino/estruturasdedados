#include "TAB_TABB.h"

void main(main)
{
	/*
	//PROBLEMA DA CRIAÇÃO DA ARVORE A PARTIR DO VETOR - NÃO SEI QUAL PROBELMA
	int vet[10] = {2, 5, 6, 10, 34, 65, 3, 4, 90, 78};
	TAB *comVetor = v2a(vet, 10);
	printf("Arvore com vetor foi cirada.\n");
	*/

	TAB *umPorUm = NULL;

	umPorUm = insere_abb(2, umPorUm);
	umPorUm = insere_abb(5, umPorUm);
	umPorUm = insere_abb(6, umPorUm);
	umPorUm = insere_abb(10, umPorUm);
	umPorUm = insere_abb(34, umPorUm);
	umPorUm = insere_abb(65, umPorUm);
	umPorUm = insere_abb(3, umPorUm);
	umPorUm = insere_abb(4, umPorUm);
	umPorUm = insere_abb(90, umPorUm);
	umPorUm = insere_abb(78, umPorUm);

	printf("\nImprime a arvore na ordem simetrica:\n");
	imp_sim(umPorUm);

	printf("\nCopia arvore: \n");
	TAB *copiarArvore = copia(umPorUm);
	imp_sim(copiarArvore);

	printf("\n---- FIM -----\n");
}